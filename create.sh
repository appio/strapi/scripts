# ==================================================== #
#  FUNCTIONS AND GLOBAL VARIABLES
# ==================================================== #

set -e
status () { echo -e "\n${BLUE}➡️  $@${NC}"; }
prompt () { echo -ne "\n${BLUE}$@${NC}"; }
error () { echo -e "\n${RED}Error: ${NC}${1}"; exit 1; }
json() { python3 -c "import sys, json; print(json.load(sys.stdin)['${2}'])" < "$1"; }

BLUE='\033[1;36m'
YELLOW='\033[1;33m'
RED='\033[1;31m'
NC='\033[0m'

# ==================================================== #
#  APPIO LOGO
# ==================================================== #

echo -e "\n   __ _ _ __  _ __ ( ) ___   ${NC}"
echo -e "  / _\` | '_ \| '_ \| |/ _ \ ${NC}"
echo -e " | (_| | |_) | |_) | | (_) | ${NC}"
echo -e "  \__,_| .__/| .__/|_|\___/  ${NC}"
echo -e "       |_|   |_|             ${NC}"
echo -e "     Strapi script 2.0.0  ${NC}"

# ==================================================== #
#  SSH KEY AND USER INSTRUCTIONS
# ==================================================== #

status "Please create a database and GIT repository manually and press enter... \c"
read

# ssh-keygen -b 2048 -t ed25519 -f "$HOME/.ssh/id_ed25519" -N "" -q

# status "Please add the following public key to the repository"
# cat "$HOME/.ssh/id_ed25519.pub"

# ==================================================== #
#  GETTING CONFIGURATION INTERACTIVELY
# ==================================================== #

prompt "GIT REPOSITORY URL: "
read GIT_REPO
[ -z "${GIT_REPO}" ] && error "GIT repository is mandatory"

prompt "GIT BRANCH [main]: "
read GIT_BRANCH
GIT_BRANCH=${GIT_BRANCH:-main}

prompt "PORT NUMBER [1337]: "
read PORT
PORT=${PORT:-1337}

cd "$HOME"
mkdir -p strapi
RELEASE_DIR="$HOME/strapi"

# ==================================================== #
#  UPDATING PATH AND .bash_profile
# ==================================================== #

status "Updating PATH and .bash_profile"

export "PATH=/opt/plesk/node/14/bin:${PATH}"
export "PATH=${HOME}/.yarn/bin:${PATH}"

echo "export PATH=/opt/plesk/node/14/bin:\$PATH" >> "${HOME}/.bash_profile"
echo "export PATH=${HOME}/.yarn/bin:\$PATH" >> "${HOME}/.bash_profile"

# ==================================================== #
#  CLONING GIT REPOSITORY
# ==================================================== #

status "Cloning GIT repository"

git clone -b "${GIT_BRANCH}" --depth 1 "${GIT_REPO}" "${RELEASE_DIR}"

# ==================================================== #
#  INSTALLING STRAPI
# ==================================================== #

status "Installing Strapi"

rm -rf "${RELEASE_DIR}/*"
yarn --cwd "$RELEASE_DIR" create strapi-app strapi 2> >(grep -v warning 1>&2)

# ==================================================== #
#  UPDATING SERVER CONFIG
# ==================================================== #

status "Updating config/server.js"

sed -i "s/1337/${PORT}/" "${RELEASE_DIR}/config/server.js"

# ==================================================== #
#  PUSHING CHANGES
# ==================================================== #

status "Pushing changes"

git config --global user.email "eppy@appio.cz"
git config --global user.name "Eppy The Developer"

cd "$RELEASE_DIR"

git add --all
git commit -m "Initial commit"
git push origin "$GIT_BRANCH"

# ==================================================== #
#  INSTALLING pm2
# ==================================================== #

status "Installing pm2"

yarn global add pm2

cp "${HOME}/setup/ecosystem.config.dev.js" "$RELEASE_DIR/ecosystem.config.js"

sed -i "s/\[APP_NAME\]/strapi/" "${RELEASE_DIR}/ecosystem.config.js"

cd "$RELEASE_DIR"
pm2 start ecosystem.config.js

pm2 update

# ==================================================== #
#  DONE
# ==================================================== #

status "Done!"

# TODO systemctl